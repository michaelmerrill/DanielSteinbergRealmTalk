//
//  HandModel.swift
//  DanielSteinbergRealmTalk
//
//  Created by Michael Merrill on 10/15/16.
//  Copyright © 2016 michaelmerrill.io. All rights reserved.
//

import Foundation
import UIKit

struct Hand: DataType {
    
    private let deck: Deck
    private var cards: [Card]
    
    init() {
        self.deck = Deck()
        self.cards = [Card]()
    }
    
    private init(deck: Deck, cards: [Card]) {
        self.deck = deck
        self.cards = cards
    }
    
    subscript(index: Int) -> Card {
        return cards[index]
    }

    var numberOfItems: Int {
        return cards.count
    }

    func addItem(atIndex index: Int) -> Hand {
        return insert(card: deck.nextCard(), atIndex: index)
    }

    private func insert(card: Card, atIndex index: Int) -> Hand {
        var mutableCards = cards
        mutableCards.insert(card, at: index)
        return Hand(deck: deck, cards: mutableCards)
    }

    func deleteItem(atIndex index: Int) -> Hand {
        var mutableCards = cards
        mutableCards.remove(at: index)
        return Hand(deck: deck, cards: mutableCards)
    }

    func moveItem(fromIndex: Int, toIndex: Int) -> Hand {
        return deleteItem(atIndex: fromIndex)
            .insert(card: cards[fromIndex], atIndex: toIndex)
    }
}
