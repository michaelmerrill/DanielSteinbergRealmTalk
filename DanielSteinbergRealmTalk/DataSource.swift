//
//  DataSource.swift
//  DanielSteinbergRealmTalk
//
//  Created by Michael Merrill on 10/16/16.
//  Copyright © 2016 michaelmerrill.io. All rights reserved.
//

import Foundation
import UIKit

protocol DataType {
    var numberOfItems: Int { get }
    func addItem(atIndex index: Int) -> Self
    func deleteItem(atIndex index: Int) -> Self
    func moveItem(fromIndex: Int, toIndex: Int) -> Self
}

protocol SourceType: UITableViewDataSource {
    var dataObject: DataType { get set }
    var conditionForAdding: Bool { get }
    
    func insertTopRow(inTableView tableView: UITableView)
    func deleteRow(atIndexPath indexPath: IndexPath, from tableView: UITableView)
}

extension SourceType {
    func insertTopRow(inTableView tableView: UITableView) {
        tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
    }
    
    func deleteRow(atIndexPath indexPath: IndexPath, from tableView: UITableView) {
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    func addItem(toTableView tableView: UITableView) {
        if conditionForAdding {
            dataObject = dataObject.addItem(atIndex: 0)
            insertTopRow(inTableView: tableView)
        }
    }
}

class DataSource: NSObject, UITableViewDataSource, SourceType {
    var dataObject: DataType = Hand()
    
    init<A: DataType>(dataObject: A) {
        self.dataObject = dataObject
    }
    
    var conditionForAdding: Bool {
        return false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataObject.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        fatalError("This method must be overriden")
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            dataObject = dataObject.deleteItem(atIndex: indexPath.row)
            deleteRow(atIndexPath: indexPath, from: tableView)
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        dataObject = dataObject.moveItem(fromIndex: fromIndexPath.row, toIndex: to.row)
    }
}
