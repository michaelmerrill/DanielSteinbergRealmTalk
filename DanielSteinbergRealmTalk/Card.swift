//
//  Card.swift
//  DanielSteinbergRealmTalk
//
//  Created by Michael Merrill on 10/15/16.
//  Copyright © 2016 michaelmerrill.io. All rights reserved.
//

import Foundation
import UIKit

struct Card {
    var rank = "Ace"
    var suit = "Hearts"
    var color = UIColor.red
}
