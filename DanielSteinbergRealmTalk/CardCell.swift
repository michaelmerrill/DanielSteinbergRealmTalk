//
//  CardCell.swift
//  DanielSteinbergRealmTalk
//
//  Created by Michael Merrill on 10/16/16.
//  Copyright © 2016 michaelmerrill.io. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    
    @IBOutlet weak var suitLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!

    func fill(withCard card: Card) {
        rankLabel.text = card.rank
        suitLabel.text = card.suit
        rankLabel.textColor = card.color
        suitLabel.textColor = card.color
    }
}
