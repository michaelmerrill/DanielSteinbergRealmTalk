//
//  HandVC.swift
//  DanielSteinbergRealmTalk
//
//  Created by Michael Merrill on 10/15/16.
//  Copyright © 2016 michaelmerrill.io. All rights reserved.
//

import UIKit

class HandVC: UITableViewController {
    
    private var dataSource = HandDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataSource
         self.navigationItem.leftBarButtonItem = self.editButtonItem
    }

    @IBAction private func addNewCard(sender: UIBarButtonItem) {
        dataSource.addItem(toTableView: tableView)
    }
}
